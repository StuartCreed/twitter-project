/**
 * Creates a input buttons (checkbox and radio)
 *
 * 'values' need to be nested e.g. values = [['Terraced House', 'checked', 'terrHsBtn', false], ['Detached', 'unchecked', 'detachBtn', true]]
 * 'values' is in format: [['title','checked/unchecked', 'id','true (optional - if you would like it disabled mark as true)', string (optional - text you would like to show in the tooltip if the label is disabled)], ['title','checked/unchecked', 'id','true (optional - if you would like it disabled mark as true)', string (optional - text you would like to show in the tooltip if the label is disabled)]...]
 * Insert 'checked' in you want the radio checked
 * 'type' can be 'radio' or 'checkbox'
 */

export default class BuildInpBtns {
    constructor(title, parentElement, values, type, wrappedAfterTwo = false, updateFunction = null) {
        this.title = title;
        this.parentElement = parentElement;
        this.values = values;
        this.type = type;
        this.wrappedAfterTwo = wrappedAfterTwo;
        this.updateFunction = updateFunction;
    }

    init() {
        const inputWrap = document.createElement('DIV');
        inputWrap.setAttribute('class', 'inp-btn-wrapper');

        const inputTitle = document.createElement('p');
        inputTitle.innerText = this.title;
        inputWrap.appendChild(inputTitle);

        this.values.forEach((val, i) => {
            const input = document.createElement('input');
            input.setAttribute('type', this.type);
            if (val[1] === 'checked') {
                input.setAttribute('checked', 'true');
            }
            input.classList.add('checked');
            input.addEventListener('click', event => this.toggleChecked(event))
            input.setAttribute('id', val[2]);
            input.setAttribute('name', this.title);
            inputWrap.appendChild(input);

            const inpLab = document.createElement('label');
            inpLab.setAttribute('for', val[2]);
            inpLab.innerText = val[0];
            inputWrap.appendChild(inpLab);

            if (!val[3] && val[3] !== undefined) {
                input.setAttribute('disabled', true);
                inpLab.classList.add('disabled');
                if (val[4] !== undefined) {
                    tippy(inpLab, {
                        content: val[4]
                    });
                }
            }

            // if wrappedAfterTwo is true then put check boxes on a new line after every two checkboxes.
            if (i % 2 === 1 && this.wrappedAfterTwo) {
                const space = document.createElement('p');
                inputWrap.appendChild(space);
            }
        })

        this.parentElement.appendChild(inputWrap);
    }

    toggleChecked (event) {
        if (this.type === 'checkbox') {
            if (event.target.classList.contains('checked')) {
                event.target.classList.remove('checked')
                event.target.classList.add('unChecked')
                this.updateFunction(event.target.id, 'unChecked')
            }
            else {
                event.target.classList.remove('unChecked')
                event.target.classList.add('checked')
                this.updateFunction(event.target.id, 'checked')
            }
        } else {
            this.updateFunction(event.target.id)
        }

    }
}
