<?php

namespace Database\Seeders;

use App\Models\Post;
use App\Models\PostTag;
use App\Models\Profile;
use App\Models\Tag;
use App\Models\User;
use Illuminate\Database\Seeder;
use \App\Models\Experience;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        User::factory(10)->create();
        Profile::factory(10)->create();
        Experience::factory(10)->create();
        Post::factory(10)->create();
        Tag::factory(10)->create();
        PostTag::factory(50)->create();
    }
}
